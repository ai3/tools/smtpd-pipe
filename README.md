smtpd-pipe
===

A very minimal SMTP server that will only forward messages to shell
commands via UNIX pipes. Its intended usage is to provide an SMTP
endpoint for containerized services that expect to receive email over
a pipe, without having to run a full MTA.

# Usage

The daemon can be configured with a list of regular expression / shell
command-line pairs: if a message matches a regular expression, the
associated command will be executed. Messages that do not match
anything will be rejected.

Rules (regexp / command pairs) are encoded as a colon-separated string
pair (which implies that the regexp can't contain a colon). They can
be passed to the program via the command-line option *--rule*
(repeated multiple times), or via the environment variable
*SMTP_RULES*, which should be a semicolon-separated list of rules.

Regular expressions are automatically anchored, so they should match
the full recipient address, including the domain part (but there is no
need to include the *^* and *$* anchors). Submatches can be used and
referenced in the command using the `$N` syntax, e.g.:

```
smtpd --rule='archive+(.*)@example.com:/usr/local/bin/archive $1'
```

Further command-line options are available to set network timeouts and
various execution limits, see *smtpd --help* for details.
