FROM golang:latest AS build
ADD . /go/src
WORKDIR /go/src
RUN go build -tags netgo -o /smtpd smtpd.go && strip /smtpd

FROM scratch
COPY --from=build /smtpd /
